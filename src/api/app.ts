import express from 'express';
import bodyParser from 'body-parser';

import {
  initProducts,
  deleteProducts,
  createProduct,
  getProductById,
  updateProductById,
  deleteProductById,
  getAllProducts,
} from '@src/functions/products';

export const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const router = express.Router();

router.route('/init').get(initProducts).delete(deleteProducts);

router.route('/products').get(getAllProducts).post(createProduct);
router
  .route('/products/:productId')
  .get(getProductById)
  .post(updateProductById)
  .delete(deleteProductById);

app.use('/api', router);
