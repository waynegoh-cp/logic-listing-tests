import { handlerPath } from '@libs/handlerResolver';

const productSchema = {
  type: 'object',
  properties: {
    name: { type: String },
    code: { type: String },
    description: { type: String },
    price: { type: Number },
  },
  required: ['name', 'code', 'price'],
};

export default {
  handler: `${handlerPath(__dirname)}/handler.main`,
  events: [
    {
      http: {
        method: 'get',
        path: '/api/init',
      },
    },
    {
      http: {
        method: 'delete',
        path: '/api/init',
      },
    },
    {
      http: {
        method: 'get',
        path: '/api/products',
      },
    },
    {
      http: {
        method: 'post',
        path: '/api/products',
        request: {
          schemas: {
            'application/json': {
              ...productSchema,
              required: ['name', 'code', 'price'],
            },
          },
        },
      },
    },
    {
      http: {
        method: 'get',
        path: '/api/products/{productId}',
      },
    },
    {
      http: {
        method: 'post',
        path: '/api/products/{productId}',
      },
    },
    {
      http: {
        method: 'delete',
        path: '/api/products/{productId}',
      },
    },
  ],
};
