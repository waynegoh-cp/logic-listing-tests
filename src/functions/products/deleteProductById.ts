import { connectToDB } from '@services/mongoDB';

import { Product } from '@src/models/Product';
import { HttpFunction } from '@src/types';

export const deleteProductById: HttpFunction = async (req, res) => {
  try {
    await connectToDB();

    const { productId } = req.params;

    const result = await Product.deleteOne({ productId });
    if (result.deletedCount) {
      return res.status(204).send();
    }

    return res.status(404).json({ message: 'Product not found!' });
  } catch (e) {
    return res.status(400).json({ message: e.message });
  }
};
