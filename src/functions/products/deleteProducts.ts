import { connectToDB } from '@services/mongoDB';

import { Product } from '@src/models/Product';
import { HttpFunction } from '@src/types';

export const deleteProducts: HttpFunction = async (req, res) => {
  try {
    await connectToDB();

    await Product.collection.drop();

    return res.status(204).send();
  } catch (e) {
    return res.status(400).json({ message: e.message });
  }
};
