import * as R from 'ramda';
import { connectToDB } from '@services/mongoDB';

import { Product } from '@src/models/Product';
import { HttpFunction } from '@src/types';

const Limit = 50;
const RequiredFields = ['productId', 'name', 'code', 'price'];

export const getAllProducts: HttpFunction = async (req, res) => {
  try {
    await connectToDB();

    const { page: reqPage = '', showId = false } = req.query;
    const page = Number(reqPage) > 1 ? Number(reqPage) : 1;

    const { docs, totalDocs, totalPages } = await Product.paginate(
      {}, // All documents
      {
        page,
        limit: Limit,
        select: RequiredFields,
        sort: { createdAt: 'desc' },
      }
    );

    const refinedFields = showId ? RequiredFields : R.drop(1, RequiredFields);

    const refineDocs = R.map(R.pickAll(refinedFields));

    return res.json({
      docs: refineDocs(docs),
      totalDocs,
      totalPages,
    });
  } catch (e) {
    return res.status(400).json({ message: e.message });
  }
};
