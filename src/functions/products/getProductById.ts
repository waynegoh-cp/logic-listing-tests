import { connectToDB } from '@services/mongoDB';

import { Product } from '@src/models/Product';
import { HttpFunction } from '@src/types';

export const getProductById: HttpFunction = async (req, res) => {
  try {
    await connectToDB();

    const { productId } = req.params;

    const product = await Product.findOne({ productId });
    if (product) {
      return res.json(product);
    }

    return res.status(404).json({ message: 'Product not found!' });
  } catch (e) {
    return res.status(400).json({ message: e.message });
  }
};
