export interface ICampaignTemplate {
  campaignTemplate: {
    _id: string;
  };
  id?: any;
}

export interface IAttribute {
  isRequired: boolean;
  entity: string;
  key: string;
}

export interface IProductBase {
  name: string;
  description: string;
  code: string;
  price: number;
}

export interface IProduct extends IProductBase {
  campaignTemplates: ICampaignTemplate[];
  attributes: IAttribute[];
}

export interface IProductAPI {
  products: IProduct[];
  count: number;
}
