import { v4 as uuid } from 'uuid';
import { connectToDB } from '@services/mongoDB';

import { Product } from '@src/models/Product';
import { IProduct } from './types';

interface IUploadProducts {
  (products: IProduct[]): Promise<number>;
}

export const uploadProducts: IUploadProducts = async products => {
  await connectToDB();

  const result: any[] = await Promise.all(
    products.map(product => Product.create({ ...product, productId: uuid() }))
  );

  const success = result.filter(res => !!res);

  return success.length;
};
