import axios from 'axios';

import { IProductAPI, IProduct } from './types';

interface IGetAllProducts {
  (page?: number, accumlated?: number): Promise<IProduct[]>;
}

const fetchProductAPI = (page: number) =>
  axios
    .get(process.env.PRODUCT_API, {
      headers: {
        apikey: process.env.PRODUCT_API_KEY,
        'content-type': 'application/json',
      },
      params: {
        page,
      },
    })
    .then(res => res.data);

export const fetchAllProducts: IGetAllProducts = async (
  page = 1,
  accumlated = 0
) => {
  try {
    const { products, count }: IProductAPI = await fetchProductAPI(page);

    const productsCount = products.length;
    const totalFetched = productsCount + accumlated;

    if (totalFetched < count) {
      const nextPage = page + 1;
      const res = await fetchAllProducts(nextPage, totalFetched);
      return [...products, ...res];
    }

    return products;
  } catch (e) {
    console.error(e);
  }

  return [];
};
