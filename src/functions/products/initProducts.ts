import { HttpFunction } from '@src/types';

import { fetchAllProducts } from './fetchAllProducts';
import { uploadProducts } from './uploadProducts';

export const initProducts: HttpFunction = async (req, res) => {
  try {
    const loaded = await fetchAllProducts();
    const saved = await uploadProducts(loaded);

    return res.json({ saved, loaded: loaded.length });
  } catch (e) {
    return res.status(400).json({ message: e.message });
  }
};
