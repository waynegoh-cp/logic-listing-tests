import * as R from 'ramda';
import { v4 as uuid } from 'uuid';
import { connectToDB } from '@services/mongoDB';

import { Product } from '@src/models/Product';
import { HttpFunction } from '@src/types';
import { pickProductData } from './utils';

export const createProduct: HttpFunction = async (req, res) => {
  try {
    await connectToDB();

    const data = pickProductData(req.body);

    const isEmptyData = R.isEmpty(data);
    if (isEmptyData) {
      return res.status(400).json({ message: 'Missing product details!' });
    }

    const dataToSave = { ...data, productId: uuid() };
    const product = await Product.create(dataToSave);
    if (product) {
      return res.json({
        data: product,
        message: 'Product successfully created!',
      });
    }

    return res.status(400).json({ message: 'Error creating new product' });
  } catch (e) {
    return res.status(400).json({ message: e.message });
  }
};
