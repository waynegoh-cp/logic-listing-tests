import * as R from 'ramda';
import { connectToDB } from '@services/mongoDB';

import { Product } from '@src/models/Product';
import { HttpFunction } from '@src/types';
import { pickProductData } from './utils';

export const updateProductById: HttpFunction = async (req, res) => {
  try {
    await connectToDB();

    const { productId } = req.params;
    const data = pickProductData(req.body);

    const isEmptyData = R.isEmpty(data);
    if (isEmptyData) {
      return res.status(400).json({ message: 'Missing product details!' });
    }

    const product = await Product.findOneAndUpdate({ productId }, data, {
      upsert: true,
      useFindAndModify: false,
    });

    if (product) {
      return res.json(product);
    }

    return res.status(404).json({ message: 'Product not found!' });
  } catch (e) {
    return res.status(400).json({ message: e.message });
  }
};
