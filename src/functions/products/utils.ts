import * as R from 'ramda';

import { IProductBase } from './types';

export const isNilOrEmpty = R.anyPass([R.isNil, R.isEmpty]);

export const isNotNilOrEmpty = R.pipe(isNilOrEmpty, R.not);

export const pickProductData = R.pipe(
  R.pickAll(['name', 'code', 'description', 'price']),
  R.pickBy(isNotNilOrEmpty)
) as (data: object) => Partial<IProductBase>;
