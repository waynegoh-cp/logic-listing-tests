import type { Request, Response } from 'express';

export interface HttpFunction {
  (req: Request, res: Response): any;
}
