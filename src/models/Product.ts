import { Schema, model } from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';

import { PaginateModel, Document } from 'mongoose';
import { IProduct } from '@functions/products/types';

interface IProductDocument extends IProduct, Document {}
interface IProductModel<T extends Document> extends PaginateModel<T> {}

const CampaignTemplatesSchema = new Schema({
  campaignTemplate: {
    _id: String,
  },
  id: { type: String },
});

const AttributesSchema = new Schema({
  isRequired: { type: Boolean },
  entity: { type: String },
  key: { type: String },
});

export const ProductSchema = new Schema(
  {
    productId: { type: String },
    name: { type: String, required: true },
    code: { type: String, required: true },
    description: { type: String },
    price: { type: Number, required: true },
    campaignTemplates: [CampaignTemplatesSchema],
    attributes: [AttributesSchema],
  },
  { timestamps: true }
);

ProductSchema.plugin(mongoosePaginate);

export const Product: IProductModel<IProductDocument> = model(
  'product',
  ProductSchema
);
