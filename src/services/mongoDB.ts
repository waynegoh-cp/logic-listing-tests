import * as mongoose from 'mongoose';

export const connectToDB = async () => {
  await mongoose.connect(process.env.MONGODB_URL);

  return mongoose.connection;
};
