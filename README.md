# Serverless - AWS Node.js Express Mongoose Typescript

This project has been generated using the `aws-nodejs-typescript` template from the [Serverless framework](https://www.serverless.com/).

For detailed instructions, please refer to the [documentation](https://www.serverless.com/framework/docs/providers/aws/).

## Installation instructions

Depending on your preferred package manager, follow the instructions below to deploy your project.

> **Requirements**: NodeJS `lts/fermium (v.14.15.0)`. If you're using [nvm](https://github.com/nvm-sh/nvm), run `nvm use` to ensure you're using the same Node version in local and in your lambda's runtime.

### Initialise Repo

- Run `yarn` or `npm i` to install the project dependencies

### MongoDB Database

You are required to setup a [MongoDB](https://www.mongodb.com/atlas/database) to allow the serverless on read/write the records

### Setup Environment Keys

- You will need to generate an `.env` file to store these keys:
  ```
  PRODUCT_API={url endpoint for products source}
  PRODUCT_API_KEY={apikey for retrieve the data from products endpoint}
  MONGODB_URL={mongoDB uri, should with Username & Password}
  ```

## Serverless instructions

### Run Locally

- Run `yarn start` to build and run serverless locally
- Serverless will be serve at `http://localhost:4000`

### Deploy to AWS

- You are required to setup [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-welcome.html) on your machine in order to deploy serverless to cloud
- Run `yarn deploy` to build and will generate following API endpoints for you to use as such:
  ```
  GET - https://{APIGATEWAY_ID}.execute-api.us-east-1.amazonaws.com/dev/api/init
  DELETE - https://{APIGATEWAY_ID}.execute-api.us-east-1.amazonaws.com/dev/api/init
  GET - https://{APIGATEWAY_ID}.execute-api.us-east-1.amazonaws.com/dev/api/products
  POST - https://{APIGATEWAY_ID}.execute-api.us-east-1.amazonaws.com/dev/api/products
  GET - https://{APIGATEWAY_ID}.execute-api.us-east-1.amazonaws.com/dev/api/products/{productId}
  POST - https://{APIGATEWAY_ID}.execute-api.us-east-1.amazonaws.com/dev/api/products/{productId}
  DELETE - https://{APIGATEWAY_ID}.execute-api.us-east-1.amazonaws.com/dev/api/products/{productId}
  ```
